# call-rest-loopback4

- A datasource: ```datasources/restsample.datasource.ts``` + ```rest.config.json```
- A service ```services/restsample.service.ts```
- A controller ```controllers/restsample.controller.ts```

## Links
- https://medium.com/@rahulrkr/calling-rest-service-proxy-loopback-next-e8d23aa37cad
- https://loopback.io/doc/en/lb4/Calling-other-APIs-and-web-services.html
- https://loopback.io/doc/en/lb4/todo-tutorial-geocoding-service.html


[![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)
